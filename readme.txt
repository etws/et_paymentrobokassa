== Extension links
Permanent link: http://shop.etwebsolutions.com/eng/et-payment-robokassa.html
Support link: http://support.etwebsolutions.com/projects/et-payment-robokassa/roadmap

== Developer Information
http://doc.etwebsolutions.com/en/instruction/et-payment-robokassa/technical-information-for-the-developer

== Short Description
Payment extension Robokassa.
Allows to use payment service Robokassa (robokassa.ru) on your Magento site.

Implemented only basic features.
Additional features are available in the extension ET_PaymentRobokassaAdvanced.

== Version Compatibility
Magento CE:
1.3.x
1.4.x
1.5.x (tested in 1.5.1.0)
1.6.x (tested in 1.6.2.0)
1.7.x (tested in 1.7.0.2)
1.8.x (tested in 1.8.0.0)
1.9.x (tested in 1.9.2.2)

== Installation
http://doc.etwebsolutions.com/en/instruction/et-payment-robokassa/install