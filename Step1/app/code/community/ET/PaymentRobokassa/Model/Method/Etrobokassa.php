<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Model_Method_Etrobokassa
 */
class ET_PaymentRobokassa_Model_Method_Etrobokassa extends Mage_Payment_Model_Method_Abstract
{

    protected $_code = 'etrobokassa';
    protected $_formBlockType = 'etpaymentrobokassa/single_form';
    protected $_infoBlockType = 'etpaymentrobokassa/single_info';

    /**
     * Payment Method features
     * @var bool
     */
    protected $_isGateway = true;
    protected $_canCapture = true;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    protected $_canManageRecurringProfiles = false;

    protected $_canUseInternal = false; // Payment method will not work in admin panel order


    /**
     * Payment Method instance configuration
     * @var bool
     */
    protected $_isActive = 0;
    protected $_title;
    protected $_description;
    protected $_testMode;

    protected $_hashType;

    protected $_sMerchantLogin;
    protected $_sInvDesc;
    protected $_paymentText;
    protected $_sIncCurrLabel;
    protected $_sCulture;
    protected $_transferCurrency;

    protected $_sMerchantPassOne;
    protected $_sMerchantPassTwo;

    protected $_cartRefill;

    protected $_url = "https://auth.robokassa.ru/Merchant/Index.aspx";
    //protected $_url = "http://test.robokassa.ru/Index.aspx"; // old test url
    protected $_configRead = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->readConfig();
    }

    /**
     * Read extension configuration
     *
     * @throws Exception
     */
    protected function readConfig()
    {
        if ($this->_configRead) {
            return;
        }
        $this->_isActive = $this->getConfigData('active');
        $this->_title = $this->getConfigData('title');
        $this->_testMode = $this->getConfigDataRobo('test_mode');
        $this->_hashType = $this->getConfigDataRobo('hash_type');
        $this->_paymentText = $this->getConfigData('payment_text');
        $this->_description = Mage::helper('cms')->getBlockTemplateProcessor()->filter($this->_paymentText);
        $this->_sMerchantLogin = $this->getConfigDataRobo('sMerchantLogin');
        $this->_sInvDesc = $this->getConfigDataRobo('sInvDesc');
        $this->_sIncCurrLabel = $this->getConfigData('sIncCurrLabel');
        $this->_transferCurrency = Mage::app()->getWebsite()->getBaseCurrencyCode();
        $this->_sCulture = $this->getConfigDataRobo('sCulture');
        $passwordFieldOne = 'sMerchantPass1';
        $passwordFieldTwo = 'sMerchantPass2';
        if ($this->_testMode) {
            $passwordFieldOne = 'testModePass1';
            $passwordFieldTwo = 'testModePass2';
        }

        $this->_sMerchantPassOne = $this->getConfigDataRobo($passwordFieldOne, null, true);
        $this->_sMerchantPassTwo = $this->getConfigDataRobo($passwordFieldTwo, null, true);
        $this->_cartRefill = $this->getConfigDataRobo('cart_refill');
        $this->_configRead = true;

        return;
    }

    /**
     * To read common settings, like login and passwords.
     * all specific method setting must be read with default function
     *
     * @param string $field
     * @param int|null $storeId
     * @param bool $decrypt - for passwords
     * @return mixed|string
     */
    public function getConfigDataRobo($field, $storeId = null, $decrypt = false)
    {
        if (null === $storeId) {
            $storeId = $this->getData('store'); // we set store in ET_PaymentRobokassaAdvanced_Helper_Data
        }
        $path = 'payment/etrobokassa/' . $field;
        $result = Mage::getStoreConfig($path, $storeId);
        if ($decrypt) {
            /* @var $coreHelper Mage_Core_Helper_Data */
            $coreHelper = Mage::helper('core');
            $result = $coreHelper->decrypt($result);
        }
        return $result;
    }

    /**
     * Payment method description from configuration
     *
     * @return string
     */
    public function getDescription()
    {
        $this->readConfig();
        return $this->_description;
    }

    /**
     * Redirect URL to robokassa controller after order place
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('etrobokassa/redirect');
    }

    /**
     * Payment system URL
     *
     * @return string
     */
    public function getGateUrl()
    {
        $this->readConfig();
        return $this->_url;
    }
    
    /**
     * Check whether payment method can be used
     *
     * @param Mage_Sales_Model_Quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        return parent::isAvailable($quote);
    }

    /**
     * To check billing country is allowed for the payment method
     *
     * @param $country
     * @return bool
     */
    public function canUseForCountry($country)
    {
        if (Mage::getStoreConfig('payment/etrobokassa/allowspecific') == 1) {
            $availableCountries = explode(',', Mage::getStoreConfig('payment/etrobokassa/specificcountry'));
            if (!in_array($country, $availableCountries)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @return array
     */
    public function preparePaymentData($order)
    {
        $this->readConfig();
        /** @var ET_PaymentRobokassa_Helper_Data $helper */
        $helper = Mage::helper("etpaymentrobokassa");

        if (empty($this->_sMerchantLogin)) {
            $helper->log('Please enter login information about your Robokassa merchant in admin panel!');
        }

        if (empty($this->_sMerchantPassOne) || empty($this->_sMerchantPassTwo)) {
            $helper->log('Please enter corrects passwords about your Robokassa merchant in admin panel!');
        }

        $hash = $this->generateHash($order, true);
        $outSum = $this->getOutSum($order);

        $postData = array(
            "MrchLogin" => $this->_sMerchantLogin,
            "OutSum" => round($outSum, 2),
            "InvId" => $this->getOrderIncrementalId($order),
            "Desc" => $this->_sInvDesc,
            "SignatureValue" => $hash,
            "IncCurrLabel" => $this->_sIncCurrLabel,
            "Email" => $order->getCustomerEmail(),
            "Culture" => $this->_sCulture,
            "IsTest" => $this->_testMode,
        );
        
        if($helper->isRobokassaSolutionCloudy()){
            $postData["Receipt"] = $this->_getReceiptData($order);
        }
        
        $result = array(
            'postData' => new Varien_Object($postData),
            'order' => $order,
        );

        Mage::dispatchEvent('robokassa_prepare_payment_data', $result);
        $postData = $result['postData']->getData();

        return $postData;
    }


    /**
     * get Receipt Data for Cloudy solutions
     * 
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    protected function _getReceiptData($order)
    {
        /** @var ET_PaymentRobokassa_Helper_Data $helper */
        $helper = Mage::helper("etpaymentrobokassa");
        
        if($helper->getTaxSystem() != ET_PaymentRobokassa_Model_System_Config_Source_Tax::TAX_DISABLE){
            $receiptData = [
                "sno" => $helper->getTaxSystem()
            ];
        }
        
        $receiptData['items'] = $this->_getReceiptItemsData($order);
        $result = json_encode($receiptData);
        $result = urlencode($result);
        
        return $result;
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _getReceiptItemsData($order)
    {
        $items = [];
        /** @var ET_PaymentRobokassa_Helper_Data $helper */
        $helper = Mage::helper("etpaymentrobokassa");
        
        if ($order->getId()) {
            /** @var array $items */
            $orderItems = $order->getAllItems();

            if (is_array($orderItems) && count($orderItems)) {

                $taxType = $helper->getTaxType();
                
                /** @var Mage_Sales_Model_Order_Item $orderItem */
                foreach ($orderItems as $orderItem) {
                    /** @var Mage_Catalog_Model_Product $product */
                    $product = $orderItem->getProduct();
                    $qty = $this->_formatQty($orderItem->getQtyOrdered());
                    $item['sum'] = $this->_formatPrice($orderItem->getPrice() * $qty);
                    $item['quantity'] = $qty;
                    $item['name'] = $product->getName();
                    $item['tax'] = $taxType;
                    $items[] = $item;
                }
            }
        } else {
            $helper->log('no order with this Id!');
        }
        
        return $items;
    }
    

    /**
     * @param string|float $price
     * @return string
     */
    protected function _formatPrice($price)
    {
        return number_format($price, 2);
    }
    
    /**
     * @param string|float $qty
     * @return string
     */
    protected function _formatQty($qty)
    {
        return number_format($qty, 0);
    }
    
    /**
     * Gets orders internal or incremental id depending on the settings
     *
     * @param Mage_Sales_Model_Order $order
     * @return string|int
     */
    public function getOrderIncrementalId($order)
    {
        /** @var ET_PaymentRobokassa_Helper_Data $helper */
        $helper = Mage::helper("etpaymentrobokassa");

        if (!$helper->getIncrementalIdType()) {
            return $order->getIncrementId();
        } else {
            return $order->getId();
        }
    }

    /**
     * Check answer from Robokassa
     *
     * @param array $answer
     * @return array of errors or true
     */
    public function checkAnswerData($answer)
    {
        try {
            $errors = array();
            $this->readConfig();

            $helper = Mage::helper('etpaymentrobokassa');
            /** @var $order Mage_Sales_Model_Order */
            $order = $helper->getOrder($this->getOrderId($answer));

            $hashArray = array(
                $answer["OutSum"],
                $answer["InvId"],
                $this->_sMerchantPassTwo
            );

            $hashCurrent = $this->generateHash($hashArray);
            $correctHash = (strcmp($hashCurrent, strtoupper($answer['SignatureValue'])) == 0);

            if (!$correctHash) {
                $errors[] = "Incorrect HASH (need:" . $hashCurrent . ", got:"
                    . strtoupper($answer['SignatureValue']) . ") - fraud data or wrong secret Key";
                $errors[] = "Maybe success payment";
            }

            if ($this->_transferCurrency != $order->getOrderCurrencyCode()) {
                $outSum = round(
                    $order->getBaseCurrency()->convert($order->getBaseGrandTotal(), $this->_transferCurrency),
                    2
                );
            } else {
                $outSum = round($order->getGrandTotal(), 2);
            }

            if ($outSum != $answer["OutSum"]) {
                $errors[] = "Incorrect Amount: " . $answer["OutSum"] . " (need: " . $outSum . ")";
            }

            if (count($errors) > 0) {
                return $errors;
            }

            return (bool)$correctHash;
        } catch (Exception $e) {
            return array("Internal error:" . $e->getMessage());
        }
    }

    /**
     * @param array $answer
     * @return string
     */
    public function getOrderId($answer)
    {
        return isset($answer["InvId"]) ? $answer["InvId"] : "";
    }


    /**
     * Generates Hash string using order data to prepare an array
     * or direct from array
     *
     * @param $data Mage_Sales_Model_Order|array
     * @param bool $writeLog
     *
     * @return string
     */
    public function generateHash($data, $writeLog = false)
    {
        $hashData = array();
        if ($data instanceof Mage_Sales_Model_Order) {
            $outSum = $this->getOutSum($data, $writeLog);
            $hashData = array(
                "MrchLogin" => $this->_sMerchantLogin,
                "OutSum" => round($outSum, 2),
                "InvId" => $this->getOrderIncrementalId($data),
                "pass" => $this->_sMerchantPassOne,
            );
        } else if (is_array($data)) {
            $hashData = $data;
        }

        $hash = $this->generateHashForArray($hashData);
        return $hash;
    }


    /**
     * Generate Hash string for array
     *
     * @param array $hashData
     * @return string
     */
    public function generateHashForArray($hashData)
    {
        /** @var ET_PaymentRobokassa_Model_Hashtype $hashModel */
        $hashModel = Mage::getModel('etpaymentrobokassa/hashtype');
        $hashList = $hashModel->getValueArray();

        if (!in_array($this->_hashType, $hashList)) {
            /** @var ET_PaymentRobokassa_Helper_Data $helper */
            $helper = Mage::helper("etpaymentrobokassa");
            $helper->log($helper->__('Not supported hash type - %s.', $this->_hashType));
        }

        $hash = strtoupper(hash($this->_hashType , implode(":", $hashData)));

        return $hash;
    }


    /**
     * Get order amount in Base Currency
     *
     * @param $order Mage_Sales_Model_Order
     * @param bool $writeLog
     *
     * @return float
     */
    public function getOutSum($order, $writeLog = false)
    {
        $displayCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $baseCurrencyCode = Mage::app()->getBaseCurrencyCode();

        if ($writeLog && ($displayCurrencyCode != $baseCurrencyCode)) {
            /** @var ET_PaymentRobokassa_Helper_Data $helper */
            $helper = Mage::helper("etpaymentrobokassa");
            $helper->log('Order amount will be transferred to service Robokassa in base currency.');
        }
        return $order->getBaseGrandTotal();
    }

}