<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Model_Paysystem
 */
class ET_PaymentRobokassa_Model_Paysystem extends Mage_Core_Model_Abstract
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        /** @var $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper('etpaymentrobokassa');
        // phpcs think that Cyrillic symbols are to long
        // @codingStandardsIgnoreStart
        $data = array(
            array('value' => "", 'label' => $helper->__('Any')),
            array('value' => "Qiwi50OceanRM", 'label' => $helper->__("Платежные терминалы QIWI Кошелек")),
            array('value' => "TerminalsElecsnetOceanR", 'label' => $helper->__("Терминалы самообслуживания Элекснет")),
            array('value' => "YandexMerchantOceanR", 'label' => $helper->__("Яндекс.Деньги")),
            array('value' => "WMRM", 'label' => $helper->__("Webmoney WMR (Рубли)")),
            array('value' => "WMZM", 'label' => $helper->__("Webmoney WMZ (Доллары)")),
            array('value' => "WMEM", 'label' => $helper->__("Webmoney WME (Евро)")),
            array('value' => "W1OceanR", 'label' => $helper->__("Платежный сервис Единый кошелек")),
            array('value' => "ElecsnetWalletR", 'label' => $helper->__("Платежный сервис Элекснет Кошелек")),
            array('value' => "W1UniMoneyOceanR", 'label' => $helper->__("RUR Единый кошелек")),
            array('value' => "Qiwi50OceanRM", 'label' => $helper->__("Электронный QIWI Кошелек")),
            array('value' => "BANKOCEAN2R", 'label' => $helper->__("Интернет-Банк Океан")),
            array('value' => "SberBankR", 'label' => $helper->__("СберБанк")),
            array('value' => "AlfaBankOceanR", 'label' => $helper->__("Интернет-Банк Альфа-Банк")),
            array('value' => "RussianStandardBankR", 'label' => $helper->__("Банк Русский Стандарт")),
            array('value' => "PSKBR", 'label' => $helper->__("Интернет-Банк Промсвязьбанк")),
            array('value' => "OceanBankOceanR", 'label' => $helper->__("Интернет-Банк Океан (platezh.ru)")),
            array('value' => "HandyBankMerchantOceanR", 'label' => $helper->__("Система интернет-банкинга HandyBank")),
            array('value' => "HandyBankBB", 'label' => $helper->__("Банк Богородский")),
            array('value' => "HandyBankFB", 'label' => $helper->__("ФлексБанк")),
            array('value' => "HandyBankFU", 'label' => $helper->__("ФьsючерБанк")),
            array('value' => "HandyBankKB", 'label' => $helper->__("КранБанк")),
            array('value' => "HandyBankKSB", 'label' => $helper->__("Костромаселькомбанк")),
            array('value' => "HandyBankLOB", 'label' => $helper->__("Липецкий областной банк")),
            array('value' => "HandyBankNSB", 'label' => $helper->__("Независимый строительный банк")),
            array('value' => "HandyBankTB", 'label' => $helper->__("Русский Трастовый Банк")),
            array('value' => "HandyBankVIB", 'label' => $helper->__("ВестИнтерБанк")),
            array('value' => "MINBankR", 'label' => $helper->__("Московский Индустриальный Банк")),
            array('value' => "BSSIntezaR", 'label' => $helper->__("Банк Интеза")),
            array('value' => "FacturaR", 'label' => $helper->__("Платежный сервис Factura.ru")),
            array('value' => "MobicomMtsR", 'label' => $helper->__("МТС")),
            array('value' => "MobicomBeelineR", 'label' => $helper->__("Билайн")),
            array('value' => "RapidaOceanSvyaznoyR", 'label' => $helper->__("Салоны сети Связной")),
            array('value' => "RapidaOceanEurosetR", 'label' => $helper->__("Салоны сети Евросеть")),
            array('value' => "BANKOCEAN2CHECKR", 'label' => $helper->__("Мобильная ROBOKASSA")),
        );
        // @codingStandardsIgnoreEnd
        return $data;
    }
}