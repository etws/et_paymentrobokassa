<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Model_Hashtype
 */
class ET_PaymentRobokassa_Model_Hashtype extends Mage_Core_Model_Abstract
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        /** @var $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper('etpaymentrobokassa');
        $data = array(
            array('value' => "md5", 'label' => $helper->__('MD5')),
            array('value' => "ripemd160", 'label' => $helper->__('RIPEMD160')),
            array('value' => "sha1", 'label' => $helper->__('SHA1')),
            array('value' => "sha256", 'label' => $helper->__('SHA256')),
            array('value' => "sha384", 'label' => $helper->__('SHA384')),
            array('value' => "sha512", 'label' => $helper->__('SHA512'))
        );
        return $data;
    }

    /**
     * @return array
     */
    public function getValueArray()
    {
        $data = $this->toOptionArray();

        $return = array();
        foreach ($data as $value) {
            array_push($return, $value['value']);
        }
        return $return;
    }
}