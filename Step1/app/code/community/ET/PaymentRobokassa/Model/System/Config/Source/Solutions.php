<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Model_System_Config_Source_Solutions
 */
class ET_PaymentRobokassa_Model_System_Config_Source_Solutions
{
    const LOYALTY_OPTION = 1;
    const CLOUDY_OPTION = 2;

    /**
     * get solutions from Robokassa
     * 
     * @return array
     */
    public function toOptionArray()
    {
        $option = array();

        $helper = Mage::helper('etpaymentrobokassa');

        $option[] = array(
            'label' => $helper->__('Loyal'),
            'value' => self::LOYALTY_OPTION
        );

        $option[] = array(
            'label' => $helper->__('Cloud'),
            'value' => self::CLOUDY_OPTION
        );
        
        return $option;
    }
}