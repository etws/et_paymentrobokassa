<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Model_System_Config_Source_Tax_System
 */
class ET_PaymentRobokassa_Model_System_Config_Source_Tax_System
{
    
    const TAX_OSN = 'osn';
    const TAX_USN_INCOME = 'usn_income';
    const TAX_USN_INCOME_OUTCOME = 'usn_income_outcome';
    const TAX_ENVD = 'envd';
    const TAX_ESN = 'esn';
    const TAX_PATENT = 'patent';

    /**
     * get Tax system
     * 
     * @return array
     */
    public function toOptionArray()
    {
        $option = array();

        $helper = Mage::helper('etpaymentrobokassa');

        $option[] = array(
            'label' => $helper->__('Total ST'),
            'value' => self::TAX_OSN
        );

        $option[] = array(
            'label' => $helper->__('Simplified ST (income)'),
            'value' => self::TAX_USN_INCOME
        );

        $option[] = array(
            'label' => $helper->__('Simplified ST (income minus costs)'),
            'value' => self::TAX_USN_INCOME_OUTCOME
        );

        $option[] = array(
            'label' => $helper->__('Single tax on imputed income'),
            'value' => self::TAX_ENVD
        );
        
        $option[] = array(
            'label' => $helper->__('Single agricultural tax'),
            'value' => self::TAX_ESN
        );

        $option[] = array(
            'label' => $helper->__('Patent ST'),
            'value' => self::TAX_PATENT
        );

        return $option;
    }
}