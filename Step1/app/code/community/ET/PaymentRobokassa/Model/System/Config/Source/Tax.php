<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Model_System_Config_Source_Tax
 */
class ET_PaymentRobokassa_Model_System_Config_Source_Tax
{
    const TAX_DISABLE = 'disable';
    const TAX_NONE = 'none';
    const TAX_VAT0 = 'vat0';
    const TAX_VAT10 = 'vat10';
    const TAX_VAT18 = 'vat18';
    const TAX_VAT110 = 'vat110';
    const TAX_VAT118 = 'vat118';

    /**
     * get tax options
     * 
     * @return array
     */
    public function toOptionArray()
    {
        $option = array();

        $helper = Mage::helper('etpaymentrobokassa');

        $option[] = array(
            'label' => $helper->__("Don't add this parameter"),
            'value' => self::TAX_DISABLE
        );
        
        $option[] = array(
            'label' => $helper->__('Without VAT'),
            'value' => self::TAX_NONE
        );

        $option[] = array(
            'label' => $helper->__('VAT at the rate of 0%'),
            'value' => self::TAX_VAT0
        );

        $option[] = array(
            'label' => $helper->__('VAT at the rate of 10%'),
            'value' => self::TAX_VAT10
        );

        $option[] = array(
            'label' => $helper->__('VAT at the rate of 18%'),
            'value' => self::TAX_VAT18
        );


        $option[] = array(
            'label' => $helper->__('VAT of the check at the estimated rate of 10/110'),
            'value' => self::TAX_VAT110
        );

        $option[] = array(
            'label' => $helper->__('VAT of the check at the estimated rate of 18/118'),
            'value' => self::TAX_VAT118
        );

        return $option;
    }
    
}