<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Controller_Router
 *
 * Нужен из-за того, что платёжный сервер в любом случае
 * возвращает на один и тот же адрес, независимо
 * от успеха операции и языка.
 *
 * We need this router, because payment server always return
 * to the same url, regardless of
 * the operation success status and the language
 */
class ET_PaymentRobokassa_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    protected $_routerController;
    protected $_routerName = 'etrobokassa';
    protected $_moduleName = 'etpaymentrobokassa';

    /**
     * Option setter
     */
    public function setRouterController()
    {
        $this->_routerController = new ET_PaymentRobokassa_Controller_Router();
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();
        $this->setRouterController();
        $front->addRouter($this->_routerName, $this->_routerController);
    }


    /**
     * Redirect customer to necessary Store View
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $identifier = trim($request->getPathInfo(), '/');
        $allPath = explode("/", $identifier);

        //ne nash modulj
        if ($this->_isOurModule($allPath[0])) {
            return false;
        }

        /* to avoid /gate/ in return urls */
        $allPath[2] = $allPath[1];
        $allPath[1] = 'gate';

        if ($orderId = $request->getParam("InvId")) {
            $order = Mage::helper('etpaymentrobokassa')->getOrder($orderId);
            Mage::app()->getStore()->load($order->getStoreId());
        }

        $request->setModuleName($this->_moduleName)
            ->setControllerName(isset($allPath[1]) ? $allPath[1] : 'gate')
            ->setActionName(isset($allPath[2]) ? $allPath[2] : 'success');
        $request->setAlias(
            Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
            trim($request->getPathInfo(), '/')
        );

        return true;
    }

    /**
     * @param string $urlKey
     * @return bool
     */
    protected function _isOurModule($urlKey)
    {
        return ($urlKey != $this->_routerName);
    }
}