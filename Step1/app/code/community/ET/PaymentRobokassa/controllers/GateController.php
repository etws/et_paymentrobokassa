<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_GateController
 */
class ET_PaymentRobokassa_GateController extends Mage_Core_Controller_Front_Action
{
    const MODULENAME = "etpaymentrobokassa";
    const PAYMENTNAME = "etrobokassa";

    /**
     * Redirect Action
     *
     * @throws Exception
     */
    public function redirectAction()
    {
        /** @var  $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper("etpaymentrobokassa");
        /* @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = Mage::getSingleton('checkout/session');
        $state = Mage_Sales_Model_Order::STATE_NEW;

        $status = Mage::getStoreConfig('payment/etrobokassa/order_status');
        if (strlen($status) == 0) {
            //$status = 'pending';
            $status = Mage::getModel('sales/order')->getConfig()->getStateDefaultStatus($state);
        }

        /* @var $order Mage_Sales_Model_Order */
        $lastOrderId = $checkoutSession->getLastOrderId();
        if (empty($lastOrderId)) {
            $messageText = $helper->__('Error. Redirect to robokassa failed. No data in session about order.');
            $helper->log($messageText);

            Mage::getSingleton('core/session')->addError($messageText);
            $this->_redirect('checkout/onepage/failure');
            return;
        }
        $order = Mage::getModel('sales/order')->load($lastOrderId);
        $order->setState($state,
            $status,
            $this->__('Customer redirected to payment Gateway Robokassa'),
            false);
        $order->save();
        /** @var ET_PaymentRobokassa_Model_Method_Etrobokassa $payment */
        $payment = $order->getPayment()->getMethodInstance();
        if (!$payment) {
            $payment = Mage::getSingleton("etpaymentrobokassa/method_etrobokassa");
        }
        $dataForSending = $payment->preparePaymentData($order);

        $dataForSending = array_merge(array('Data transfer' => 'To Robokassa'), $dataForSending);
        $helper->log($dataForSending);
        $this->getResponse()->setHeader('Content-type', 'text/html; charset=UTF8');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('etpaymentrobokassa/redirect')->setGateUrl(
                $payment->getGateUrl())->setPostData($dataForSending)->toHtml()
        );
    }

    /**
     * Status Action
     *
     * @throws Exception
     */
    public function statusAction()
    {
        /** @var  $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper("etpaymentrobokassa");
        $state = Mage_Sales_Model_Order::STATE_PROCESSING;
        $errorStatus = 'error_robokassa';
        $answer = $this->getRequest()->getParams();
        /** @var ET_PaymentRobokassa_Model_Method_Etrobokassa $payment */
        $payment = Mage::getSingleton(self::MODULENAME . "/method_" . self::PAYMENTNAME);
        if ($payment->getOrderId($answer)) {
            /* @var $order Mage_Sales_Model_Order */
            $order = $helper->getOrder($payment->getOrderId($answer));
            //true un success, false on fail or array of text on error
            /** @var ET_PaymentRobokassa_Model_Method_Etrobokassa $orderPaymentMethod */
            $orderPaymentMethod = $order->getPayment()->getMethodInstance();
            $checkedAnswer = $orderPaymentMethod->checkAnswerData($answer);

            if (is_array($checkedAnswer)) {
                $answer['Errors'] = $helper->arrayToRawData($checkedAnswer);
            }
            $result = array(
                'answer' => new Varien_Object($answer),
                'order' => $order,
            );
            Mage::dispatchEvent('robokassa_success_answer', $result);
            $answer = $result['answer']->getData();
            $answer = array_merge(array('Data transfer' => 'From Robokassa'), $answer);

            $helper->log($answer);

            if ($checkedAnswer === true) {
                Mage::dispatchEvent('robokassa_success_answer_without_error', $result);
                if ($order->getState() == Mage_Sales_Model_Order::STATE_NEW) {
                    if ($order->canInvoice()) {
                        //for partial invoice
                        if (!($qtys = $orderPaymentMethod->getInvoiceQtys($order))) {
                            $qtys = array();
                        }
                        $invoice = $order->prepareInvoice($qtys);
                        $invoice->register();
                        $invoice->capture();
                        $order->addRelatedObject($invoice);
                    }
                    if (!($paidStatus = $orderPaymentMethod->getPaidStatus())) {
                        $paidStatus = 'paid_robokassa';
                    }
                    $result = $this->_sendEmailAfterPaymentSuccess($order);

                    $order->setState($state,
                        $paidStatus,
                        $this->__($helper->__('The amount has been authorized and captured by Robokassa.')),
                        $result);
                    $order->save();
                }
                print 'OK' . $orderPaymentMethod->getOrderId($answer);
            } else {
                if ($order->getId()) {
                    $historyStatusText = implode(" / ", $checkedAnswer);
                    $order->addStatusToHistory($errorStatus, $historyStatusText, false);
                    $order->save();
                    /* @var $quote Mage_Sales_Model_Quote */
                    $quote = Mage::getModel("sales/quote");
                    $quote->load($order->getQuoteId());
                    if ($quote->getId() > 0) {
                        /* @var $checkoutHelper Mage_Checkout_Helper_Data */
                        $checkoutHelper = Mage::helper('checkout');
                        $checkoutHelper->sendPaymentFailedEmail($quote, $historyStatusText);
                    }
                }
                print "Error";
            }
        } else {
            $errorAnswer = 'Incorrect answer. No order number.';
            $helper->log($errorAnswer);
            if (!$answer) {
                $answer = 'No answer data';
            }
            $helper->log($answer);
            print($errorAnswer);
            //$this->_redirect('/');
        }
    }

    /**
     * Failure action
     *
     * @throws Exception
     */
    public function failureAction()
    {
        /** @var  $helper ET_PaymentRobokassa_Helper_Data */
        $helper = Mage::helper("etpaymentrobokassa");

        /** @var ET_PaymentRobokassa_Model_Method_Etrobokassa $payment */
        $payment = Mage::getSingleton(self::MODULENAME . "/method_" . self::PAYMENTNAME);
        $answer = $this->getRequest()->getParams();

        if ($payment->getOrderId($answer)) {
            /** @var $order Mage_Sales_Model_Order */
            $order = $helper->getOrder($payment->getOrderId($answer));
            $result = array(
                'answer' => new Varien_Object($answer),
                'order' => $order,
            );

            Mage::dispatchEvent('robokassa_failure_answer', $result);
            $answer = $result['answer']->getData();
            $answer = array_merge(array('Data transfer' => 'From Robokassa'), $answer);
            $helper->log($answer);
            if (Mage::getStoreConfig('payment/' . self::PAYMENTNAME . '/cart_refill')) {
                $helper->refillCart($order);
            }

            $order->addStatusToHistory(
                $order->getStatus(),
                $helper->__('Payment failed'),
                false
            );
            $order->save();
            $order->cancel()->save();
        } else {
            $errorAnswer = 'Incorrect answer. No order number.';
            $helper->log($errorAnswer);
            if (!$answer) {
                $answer = 'No answer data';
            }
            $helper->log($answer);
            /* @var $checkoutSession Mage_Checkout_Model_Session */
            $checkoutSession = Mage::getSingleton('checkout/session');
            $checkoutSession->addError($helper->__($errorAnswer));
        }
        $this->_redirect('checkout/onepage/failure');
    }

    /**
     * Success action
     */
    public function successAction()
    {
        /** @var $session Mage_Checkout_Model_Session */
        $session = Mage::getSingleton('checkout/session');
        if (!$session->getLastOrderId() || !$session->getLastQuoteId() || !$session->getLastSuccessQuoteId()) {
            $answer = $this->getRequest()->getParams();
            Mage::dispatchEvent('robokassa_no_session_data_for_success', $answer);
        }
        $this->_redirect("checkout/onepage/success");
    }

    /**
     * Send new order email
     * @param $order Mage_Sales_Model_Order
     * @return bool
     */
    protected function _sendEmailAfterPaymentSuccess($order)
    {
        if ($order->sendNewOrderEmail()) {
            $result = true;
            $order->setEmailSent(true);
        } else {
            $result = false;
        }
        return $result;
    }

}