<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Helper_Data
 */
class ET_PaymentRobokassa_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * get Tax System
     * 
     * @return string
     */
    public function getTaxSystem()
    {
        return (string)Mage::getStoreConfig('payment/etrobokassa/tax_system');
    }
    
    /**
     * @return string
     */
    public function getTaxType()
    {
        return (string)Mage::getStoreConfig('payment/etrobokassa/tax_type');
    }
    
    /**
     * check solution is cloudy?
     * 
     * @return bool
     */
    public function isRobokassaSolutionCloudy()
    {
        $solutionsType = (int)Mage::getStoreConfig('payment/etrobokassa/solutions_type');
        if($solutionsType == ET_PaymentRobokassa_Model_System_Config_Source_Solutions::CLOUDY_OPTION){
            return true;
        }else{
            return false;
        }
    }
    
    
    /**
     * Refilling cart after failed payment
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function refillCart($order)
    {
        /* @var $cart Mage_Checkout_Model_Cart */
        $cart = Mage::getSingleton('checkout/cart');
        $items = $order->getItemsCollection();
        /* @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = Mage::getSingleton('checkout/session');
        /** @var $item Mage_Sales_Model_Order_Item */
        foreach ($items as $item) {
            try {
                $cart->addOrderItem($item);
            } catch (Exception $e) {
                $checkoutSession->addError(
                    Mage::helper('checkout')->__('Cannot add the item %s to shopping cart.', $item->getName())
                );
            }
        }
        $cart->save();
    }

    /**
     * Writes information to log file
     *
     * @param $message - string or array of strings
     * @return bool
     */
    public function log($message)
    {
        if ($this->isLogEnabled()) {
            $file = $this->getLogFileName();
            if (is_array($message)) {
                Mage::dispatchEvent('robokassa_log_file_write_before', $message);
                $forLog = array();
                foreach ($message as $answerKey => $answerValue) {
                    $answer = is_array($answerValue) ? print_r($answerValue, true) : $answerValue;
                    $forLog[] = $answerKey . ": " . $answer;
                }
                $forLog[] = '***************************';
                $message = implode("\r\n", $forLog);
            }
            Mage::log($message, Zend_Log::DEBUG, $file, true);
        }
        return true;
    }

    /**
     * Convert array to string for logging
     *
     * @param $array
     * @return string
     */
    public function arrayToRawData($array)
    {
        $newArray = array();
        foreach ($array as $key => $value) {
            $newArray[] = $key . ": " . $value;
        }
        $raw = implode("\r\n", $newArray);
        return $raw;
    }
    
    /**
     * Is log writing enabled or not?
     *
     * @return bool
     */
    public function isLogEnabled()
    {
        // for new extensions use variable "log_enabled"
        return (bool)Mage::getStoreConfig('payment/etrobokassa/enable_log');
    }

    /**
     * Function returns log file name
     *
     * @return string
     */
    public function getLogFileName()
    {
        return Mage::getStoreConfig('payment/etrobokassa/log_file');
    }


    /**
     * Extension can use two types of order id, that will be transferred to payment system Robokassa
     * 1 - order internal id (1 - 9999999) - $order->getId();
     * 0 - order incremental id (x00000001) - $order->getIncrementId() - by default
     *
     * @return int
     */
    public function getIncrementalIdType()
    {
        return (int)Mage::getStoreConfig('payment/etrobokassa/invoice_id_type');
    }

    /**
     * Gets an order by its internal or incremental id depending on the settings
     *
     * @param string|int $id
     * @return Mage_Sales_Model_Order
     */
    public function getOrder($id)
    {
        /** @var $order Mage_Sales_Model_Order */
        $order = Mage::getModel("sales/order");

        if (!$this->getIncrementalIdType()) {
            $order->loadByIncrementId($id);
        } else {
            $order->load($id);
        }
        return $order;
    }

}
