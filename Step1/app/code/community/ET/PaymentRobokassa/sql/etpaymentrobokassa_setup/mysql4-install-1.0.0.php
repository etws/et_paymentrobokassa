<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

$installStatus = version_compare(Mage::getVersion(), '1.4.2.0', '>');

if ($installStatus) {
    /* @var $installer Mage_Sales_Model_Entity_Setup */
    $installer = $this;

    $installer->startSetup();
    $statusTable = $installer->getTable('sales/order_status');
    $statusStateTable = $installer->getTable('sales/order_status_state');
    $statusLabelTable = $installer->getTable('sales/order_status_label');

    $paymentStatuses = array();
    $paymentStatuses[] = array(
        'status' => 'waiting_robokassa',
        'label' => 'Waiting Robokassa payment');
    $paymentStatuses[] = array(
        'status' => 'error_robokassa',
        'label' => 'Error data from Robokassa');
    $paymentStatuses[] = array(
        'status' => 'paid_robokassa',
        'label' => 'Paid by Robokassa');

    foreach ($paymentStatuses as $paymentStatus) {
        try {
            $installer->getConnection()->insertArray($statusTable, array('status', 'label'), array($paymentStatus));
        } catch (Exception $e) {
            //none
        }
    }

    $paymentStatusToState = array();
    $paymentStatusToState[] = array(
        'status' => 'waiting_robokassa',
        'state' => 'new',
        'is_default' => 0
    );

    $paymentStatusToState[] = array(
        'status' => 'error_robokassa',
        'state' => 'new',
        'is_default' => 0
    );

    $paymentStatusToState[] = array(
        'status' => 'paid_robokassa',
        'state' => 'processing',
        'is_default' => 0
    );

    foreach ($paymentStatusToState as $statusToState) {
        try {
            $installer->getConnection()->insertArray(
                $statusStateTable,
                array('status', 'state', 'is_default'),
                array($statusToState)
            );
        } catch (Exception $e) {
            //none
        }
    }

    $installer->endSetup();
} else {
    // statuses for old Magento versions are in config.xml
}