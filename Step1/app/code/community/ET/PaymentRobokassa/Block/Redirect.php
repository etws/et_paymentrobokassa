<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Block_Redirect
 */
class ET_PaymentRobokassa_Block_Redirect extends Mage_Core_Block_Abstract
{
    protected $_postData;

    /**
     * @return string
     */
    protected function _toHtml()
    {
        $html = '<html><body>';
        $html .= $this->__('You will be redirected to payment form in a few seconds.');
        $html .= '<form method="get" action="' . $this->getGateUrl() . '" id="gate_post_form">';
        foreach ($this->_postData as $key => $value) {
            $html .= '<input type="hidden" name="' . $key . '" value="' . $value . '">';
        }
        print '<input type="submit" value="' . $this->__("Click here, if not redirected for 30 seconds") . '">';
        $html .= '</form><script type="text/javascript">document.getElementById("gate_post_form").submit();</script>';
        $html .= '</body></html>';
        return $html;
    }

    /**
     * @param $data array
     * @return $this ET_PaymentRobokassa_Block_Redirect
     */
    public function setPostData($data)
    {
        $this->_postData = $data;
        return $this;
    }
}
