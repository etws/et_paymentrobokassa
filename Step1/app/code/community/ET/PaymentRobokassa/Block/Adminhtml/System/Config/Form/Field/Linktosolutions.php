<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Block_Adminhtml_System_Config_Form_Field_Linktosolutions
 */
class ET_PaymentRobokassa_Block_Adminhtml_System_Config_Form_Field_Linktosolutions
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        /** @var ET_PaymentRobokassa_Helper_Data $helper */
        $helper = Mage::helper('etpaymentrobokassa');

        $html = "<a target='_blank' href='https://fiscal.robokassa.ru/'>" .
            $helper->__('Choosing a Robokassa solution') . "</a><br/>";
        $html .= "<a target='_blank' href='https://docs.robokassa.ru/#6865'>" .
            $helper->__('Robokassa documentation') . "</a>";
        
        return $html;
    }
}