<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_PaymentRobokassa
 * @copyright  Copyright (c) 2015 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_PaymentRobokassa_Block_Adminhtml_System_Config_Form_Field_CallbackUrls
 */
class ET_PaymentRobokassa_Block_Adminhtml_System_Config_Form_Field_CallbackUrls
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Heading
{
    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $baseUrl = Mage::getBaseUrl('web');
        $description = "You have to set callback links for your shop in" .
            " your account on Robokassa website to make extension work.";
        $comment = "POST or GET data transfer method";
        return <<<HTML
<tr class="system-fieldset-sub-head" id="row_{$element->getHtmlId()}">
    <td colspan="5"><h4 id="{$element->getHtmlId()}">{$element->getData('label')}</h4></td>
</tr>
<tr>
    <td colspan="5"><span>
        {$this->__($description)}
    </span></td>
</tr>
<tr>
    <td class="label"><label>{$this->__("Result URL")}</label></td>
    <td colspan="4" class="value">
        {$baseUrl}etrobokassa/status
        <p class="note"><span>{$this->__($comment)}</span></p>
    </td>
</tr>
<tr>
    <td class="label"><label>{$this->__("Success URL")}</label></td>
    <td colspan="4" class="value">
        {$baseUrl}etrobokassa/success
        <p class="note"><span>{$this->__($comment)}</span></p>
    </td>
</tr>
<tr>
    <td class="label"><label>{$this->__("Failure URL")}</label></td>
    <td colspan="4" class="value">
        {$baseUrl}etrobokassa/failure
        <p class="note"><span>{$this->__($comment)}</span></p>
    </td>
</tr>
<tr>
    <td class="label"><label>{$this->__("Documentation")}</label></td>
    <td colspan="4" class="value">
        <a href="http://shop.etwebsolutions.com/eng/et-payment-robokassa.html#documentation" target="_blank">
            http://shop.etwebsolutions.com/eng/et-payment-robokassa.html#documentation
        </a>
    </td>
</tr>
HTML;
    }
}
